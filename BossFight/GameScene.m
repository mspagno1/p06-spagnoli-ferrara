//
//  GameScene.m
//  BossFight
//
//  Created by Vaster on 4/6/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameScene.h"


//Masks of the two coliding objects
static const uint32_t bossCategory =  0x1 << 0;
static const uint32_t bossFireCategory =  0x1 << 1;
static const uint32_t playerCategory =  0x1 << 2;
static const uint32_t groundCategory =  0x1 << 3;
static const uint32_t fireBallCategory = 0x1 << 4;
static const uint32_t platformCategory = 0x1 << 5;


@implementation GameScene {
    
    NSTimeInterval _lastUpdateTime;
    SKSpriteNode * background;
    SKSpriteNode * ground;
    SKSpriteNode * platform[5];
    //SKSpriteNode * platformPattern[5];
    SKSpriteNode * player;
    SKSpriteNode * fireButton;
    SKSpriteNode * fireBall;
    SKSpriteNode * resetButton;
    
    SKCameraNode * cam;
    
    
    
    //Player actions
    SKAction* jumpLeft;
    SKAction* walkLeft;
    SKAction* jumpRight;
    SKAction* walkRight;
    SKAction* death;
    SKAction* hit;
    
    SKTexture* celesHit;
    SKTexture* celesRight;
    SKTexture* celesLeft;
    
    //Can have up to 6 fireballs
    SKSpriteNode * dragonBoss;
    SKSpriteNode * bossFireBall[6];

    //Boss Health
    int bossHealth;
    
    int bossPhase;
    
    //Int player hp
    int playerHealth;
    
    //True is player left false is player is right
    Boolean playerLeft;
    
    //Timeframe counter
    int frameCounter;
    int dragonDirection;
}

- (void)sceneDidLoad {
    bossPhase = 0;
    frameCounter = 0;
    dragonDirection = -1;
    
    // Initialize update time
    _lastUpdateTime = 0;
    
    static BOOL didLoad = NO;
    
    
    //Scene is being loaded twice for some reason, camera only works on second load
    if (didLoad)
    {
        cam = [SKCameraNode node];
        self.camera = cam;
        //NSLog(@"Skipping the second load");
        return;
    }
    didLoad = YES;
    
    //Make the level
    [self makeBackground:0];
    [self makeBackground:background.size.width - 230];
    [self makeBackground:0-background.size.width + 384];
    
    [self makeGround];
    
    self.physicsWorld.gravity = CGVectorMake( 0.0, -3.0 );
    self.physicsWorld.contactDelegate = self;
    
    //Make player
    [self makePlayer];
    [self makeAnimation];
    [self makeFireButton];
    [self makeResetButton];

    [self makeBoss];
    

}

-(void) reset{
    [player removeFromParent];
    for(int i = 0; i < 3; i++){
        [platform[i] removeFromParent];
    }
    [dragonBoss removeFromParent];
    [fireBall removeFromParent];
    [self clearBossFire];
    [self makeBoss];
    [self makePlayer];
    
    bossPhase = 0;
    frameCounter = 0;
    dragonDirection = 0;
}


-(void) makeBackground:(int)xLoc{
    background = [SKSpriteNode spriteNodeWithImageNamed: @"background.png"];
    background.size = CGSizeMake(background.size.width ,self.frame.size.height);
    
    [background setPosition: CGPointMake(xLoc,0)];
    background.physicsBody.affectedByGravity = FALSE;
    //background.physicsBody.categoryBitMask = backGroundCategory;
    [self addChild: (background)];
    
    
}

-(void) makeGround{
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    ground.size = CGSizeMake(ground.texture.size.width * 1.4, ground.texture.size.height);
    
    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(ground.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    ground.physicsBody.categoryBitMask = groundCategory;
    //ground.physicsBody.contactTestBitMask = bossFireCategory;

    [self addChild: (ground)];
    
}


-(void) makePlatform {
    for(int i = 0; i < 3; i++){
        platform[i] = [SKSpriteNode spriteNodeWithImageNamed:@"Bar.png"];
        platform[i].size = CGSizeMake(150, 40);
    
        platform[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(platform[i].size.width, platform[i].size.height)];
        platform[i].physicsBody.dynamic = NO;
        platform[i].physicsBody.affectedByGravity = FALSE;
        platform[i].physicsBody.categoryBitMask = platformCategory;
        platform[i].physicsBody.contactTestBitMask = bossFireCategory | playerCategory;
        platform[i].physicsBody.usesPreciseCollisionDetection = YES;
    }
    [platform[0] setPosition:CGPointMake(-200, -380)];
    [platform[1] setPosition:CGPointMake(-600, -150)];
    [platform[2] setPosition:CGPointMake(-400, -250)];
    //[platform[3] setPosition:CGPointMake(-350, -260)];
    //[platform[4] setPosition:CGPointMake(-300, -300)];
    
    for(int i = 0; i < 3; i++){
        [self addChild: (platform[i])];
    }
}


-(void) makePlayer{
    //Set up player
    SKTexture* CelesGround = [SKTexture textureWithImageNamed:@"Celes.gif"];
    player = [SKSpriteNode spriteNodeWithTexture: CelesGround];
    
    playerLeft = false;
    [player setPosition: CGPointMake(0,-517)];
    //player.size = player.texture.size;
    player.size = CGSizeMake(80, 100);
    
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    player.physicsBody.categoryBitMask = playerCategory;
    player.physicsBody.contactTestBitMask = bossCategory | bossFireCategory;
    player.physicsBody.usesPreciseCollisionDetection = YES;
    
    playerHealth = 5;
    
    [self addChild: (player)];
}

-(void) makeBoss{
    //Set up player
    SKTexture* bossLeft = [SKTexture textureWithImageNamed:@"BossLeft.gif"];
    dragonBoss = [SKSpriteNode spriteNodeWithTexture: bossLeft];
    
    [dragonBoss setPosition: CGPointMake(400,-400)];
    //dragonBoss.size = dragonBoss.texture.size;
    dragonBoss.size = CGSizeMake(300, 500);
    
    
    dragonBoss.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:dragonBoss.size];

    dragonBoss.physicsBody.dynamic = YES;
    dragonBoss.physicsBody.allowsRotation = false;
    dragonBoss.physicsBody.affectedByGravity = FALSE;

    dragonBoss.physicsBody.categoryBitMask = bossCategory;
    dragonBoss.physicsBody.contactTestBitMask = fireBallCategory | playerCategory;
    dragonBoss.physicsBody.usesPreciseCollisionDetection = YES;
    
    bossHealth = 30;
    
    [self addChild: (dragonBoss)];
}


-(void) makeAnimation{
    
    //Walk action Left
    SKTexture* CelesWalkLeft1 = [SKTexture textureWithImageNamed:@"walkLeft1.tiff"];
    CelesWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft2 = [SKTexture textureWithImageNamed:@"walkLeft2.tiff"];
    CelesWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft3 = [SKTexture textureWithImageNamed:@"walkLeft3.tiff"];
    CelesWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft4 = [SKTexture textureWithImageNamed:@"walkLeft4.tiff"];
    CelesWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    walkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkLeft1,CelesWalkLeft2,CelesWalkLeft3,CelesWalkLeft4] timePerFrame:0.2]];
    
    celesLeft = CelesWalkLeft1;
    
    //Walk action Right
    SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"walkRight1.tiff"];
    CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"walkRight2.tiff"];
    CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"walkRight3.tiff"];
    CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"walkRight4.tiff"];
    CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
    
    celesRight = CelesWalkRight1;
    
    //Jump action Left
    SKTexture* CelesJumpLeft1 = [SKTexture textureWithImageNamed:@"jumpLeft1.png"];
    CelesJumpLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesJumpLeft2 = [SKTexture textureWithImageNamed:@"jumpLeft2.tiff"];
    CelesJumpLeft2.filteringMode = SKTextureFilteringNearest;
    
    jumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpLeft1, CelesJumpLeft2] timePerFrame:0.2]];
    
    //Jump action Right
    SKTexture* CelesJumpRight1 = [SKTexture textureWithImageNamed:@"jumpRight1.tiff"];
    CelesJumpRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesJumpRight2 = [SKTexture textureWithImageNamed:@"jumpRight2.tiff"];
    CelesJumpRight2.filteringMode = SKTextureFilteringNearest;
    
    jumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpRight1, CelesJumpRight2] timePerFrame:0.2]];
    
    //Death
    celesHit = [SKTexture textureWithImageNamed:@"CelesHit.gif"];
    celesHit.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesDeath1 = [SKTexture textureWithImageNamed:@"CelesDeath1.gif"];
    CelesDeath1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesDeath2 = [SKTexture textureWithImageNamed:@"CelesDeath2.gif"];
    CelesDeath2.filteringMode = SKTextureFilteringNearest;
    
    death = [SKAction animateWithTextures:@[celesHit, CelesDeath1,CelesDeath2] timePerFrame:0.5];
    
    hit = [SKAction animateWithTextures:@[celesHit] timePerFrame:0.2];
    
    
    //BOSS ANIMATIONS GO HERE
    
    
    
}

-(void) makeFireBall {
    [fireBall removeFromParent];
    fireBall = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
    //Fireball to the left or right of the player
    if(playerLeft == false){
        [fireBall setPosition: CGPointMake(player.position.x + 30,player.position.y +10)];
    }
    else{
        [fireBall setPosition: CGPointMake(player.position.x - 30,player.position.y + 10)];
    }
    fireBall.size = CGSizeMake(60, 60);
    
    fireBall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fireBall.texture.size];
    
    fireBall.physicsBody.affectedByGravity = false;
    fireBall.physicsBody.allowsRotation = false;
    fireBall.physicsBody.categoryBitMask = fireBallCategory;
    fireBall.physicsBody.contactTestBitMask = bossCategory | bossFireCategory;
    fireBall.physicsBody.usesPreciseCollisionDetection = YES;
    [self addChild: fireBall];
}

-(void) makeBossFire {
    [self clearBossFire];
    
    for (int i = 0; i < 6; ++i) {
        bossFireBall[i] = [SKSpriteNode spriteNodeWithImageNamed:@"Firework3.gif"];
        bossFireBall[i].size = CGSizeMake(80.0, 80.0);
        bossFireBall[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bossFireBall[i].texture.size];
        bossFireBall[i].physicsBody.affectedByGravity = false;
        bossFireBall[i].physicsBody.allowsRotation = false;
        bossFireBall[i].physicsBody.categoryBitMask = bossFireCategory;
        bossFireBall[i].physicsBody.contactTestBitMask = playerCategory | groundCategory | fireBallCategory;
        bossFireBall[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        NSString *str = @"bossFire";
        NSString* fireNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:fireNum];
        
        bossFireBall[i].name = str;
    }
}

-(void) pattern1 {
    [self makeBossFire];
    printf("Pattern one is called here");
    for (int i = 0; i < 6; ++i) {
        [bossFireBall[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
        [self addChild:bossFireBall[i]];

    }
    
    [bossFireBall[0].physicsBody applyImpulse:CGVectorMake(-10, 15)];
    [bossFireBall[1].physicsBody applyImpulse:CGVectorMake(-10, 10)];
    [bossFireBall[2].physicsBody applyImpulse:CGVectorMake(-10, 5)];
    [bossFireBall[3].physicsBody applyImpulse:CGVectorMake(-10, -5)];
    [bossFireBall[4].physicsBody applyImpulse:CGVectorMake(-10, -10)];
    [bossFireBall[5].physicsBody applyImpulse:CGVectorMake(-10, -15)];
}

-(void) pattern2 {
    [self makeBossFire];
    printf("Pattern two is called here");
    for (int i = 0; i < 6; ++i) {
        [bossFireBall[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
        [self addChild:bossFireBall[i]];
        
    }
    
    [bossFireBall[0].physicsBody applyImpulse:CGVectorMake(-10, -3)];
    [bossFireBall[1].physicsBody applyImpulse:CGVectorMake(-10, -13)];
    [bossFireBall[2].physicsBody applyImpulse:CGVectorMake(-10, -8)];
    [bossFireBall[3].physicsBody applyImpulse:CGVectorMake(-10, -5)];
    [bossFireBall[4].physicsBody applyImpulse:CGVectorMake(-10, -10)];
    [bossFireBall[5].physicsBody applyImpulse:CGVectorMake(-10, -15)];
}

-(void) pattern3 {
    [self makeBossFire];
    printf("Pattern three is called here");
    for (int i = 0; i < 6; ++i) {
        [bossFireBall[i] setPosition:CGPointMake(dragonBoss.position.x - 100, dragonBoss.position.y - 30)];
        [self addChild:bossFireBall[i]];
        
    }
    
    [bossFireBall[0].physicsBody applyImpulse:CGVectorMake(-15, 10)];
    [bossFireBall[1].physicsBody applyImpulse:CGVectorMake(-15, 5)];
    [bossFireBall[2].physicsBody applyImpulse:CGVectorMake(-15, 13)];
    [bossFireBall[3].physicsBody applyImpulse:CGVectorMake(-15, 8)];
    [bossFireBall[4].physicsBody applyImpulse:CGVectorMake(-15, 3)];
    [bossFireBall[5].physicsBody applyImpulse:CGVectorMake(-15, 0)];
}

-(void) clearBossFire {
    for(int i = 0; i < 6; i++){
        [bossFireBall[i] removeFromParent];
    }
}


//fire button
-(void) makeFireButton {
    fireButton = [SKSpriteNode spriteNodeWithImageNamed:@"button.png"];
    fireButton.size = CGSizeMake(100 ,100);
    fireButton.position = CGPointMake(player.position.x,-620);
    fireButton.name = @"fireButtonNode";//how the node is identified later
    
    [self addChild: (fireButton)];
}

//fire button
-(void) makeResetButton {
    resetButton = [SKSpriteNode spriteNodeWithImageNamed:@"reset.png"];
    resetButton.size = CGSizeMake(100 ,100);
    resetButton.position = CGPointMake(player.position.x + 270,-620);
    resetButton.name = @"reset";//how the node is identified later
    
    [self addChild: (resetButton)];
}



-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    if ([node.name isEqualToString:@"fireButtonNode"] && playerHealth > 0) {
        [self makeFireBall];
        if(playerLeft == true){
            [fireBall.physicsBody applyImpulse:CGVectorMake(-15, 0)];
        }
        else{
            [fireBall.physicsBody applyImpulse:CGVectorMake(15, 0)];
        }
    }
    
    else if ([node.name isEqualToString:@"reset"]) {
        [self reset];
    }
    
    else{
        if(playerHealth > 0){
            if(location.y <= player.position.y){
                if(location.x > player.position.x && location.y){
                    playerLeft = false;
                    [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
                
                    [player runAction:walkRight];
                
                }
                else{
                    playerLeft = true;
                    [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                    [player runAction:walkLeft];
                
                }
            //}
            }
            else{
                    if(player.physicsBody.velocity.dy == 0){
                        if(location.x > player.position.x && location.y){
                            playerLeft = false;
                            [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
                            [player runAction:jumpRight];
                            
                        }
                        else{
                            playerLeft = true;
                            [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                            [player runAction:jumpLeft];
                    
                        }
                    }
        
            }
        }
    }
    
    
}


//Physhics contact goes here

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }

    
    if ((firstBody.categoryBitMask == bossCategory)&& (secondBody.categoryBitMask == fireBallCategory))
    {
        printf("Fireball hit");
        NSLog(@"%@ \n" , firstBody.node.name);
        //How know what object to remove
        bossHealth--;
        if(bossHealth == 0){
            //Death thing here maybe
            [dragonBoss removeFromParent];
        }
        [fireBall removeFromParent];
        
    }
    
    
    if ((firstBody.categoryBitMask == bossFireCategory)&& (secondBody.categoryBitMask == fireBallCategory))
    {
        printf("Fireball colide");
        [fireBall removeFromParent];
        
        //Too strong for this to occur other bit thing to avoid these collisions?
        /*
        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFireBall[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFireBall[2] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire3"]){
            [bossFireBall[3] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire4"]){
            [bossFireBall[4] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire5"]){
            [bossFireBall[5] removeFromParent];
        }
         */
        
    }
    
    //Player hit by bosses fireball
    
    if ((firstBody.categoryBitMask == bossFireCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        printf("Collison");
        playerHealth --;
        if(playerHealth == 0){
            //gameOver
            NSLog(@"Player died");
            [player removeAllActions];
            [player runAction:death];
            

            //[player removeFromParent];
        }
        else if(playerHealth > 0){
            [player removeAllActions];

            //[player setTexture:celesHit];
            [player runAction: hit];
            [player.physicsBody applyImpulse:CGVectorMake(-5, 0)];
            
            /*
            if(playerLeft == true){
                [player setTexture: celesLeft];
            }
            else{
                [player setTexture: celesRight];
            }
            */
            

        }
        NSLog(@"Name of this is %@ \n" , firstBody.node.name);

        
        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFireBall[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFireBall[2] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire3"]){
            [bossFireBall[3] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire4"]){
            [bossFireBall[4] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire5"]){
            [bossFireBall[5] removeFromParent];
        }
        
        
    }
    
    //If fireballs hits ground it should delete
    if ((firstBody.categoryBitMask == bossFireCategory)&&
        (secondBody.categoryBitMask == groundCategory))
    {
        printf("fire ground");
        NSLog(@"%@ \n" , firstBody.node.name);

        
        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFireBall[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFireBall[2] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire3"]){
            [bossFireBall[3] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire4"]){
            [bossFireBall[4] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire5"]){
            [bossFireBall[5] removeFromParent];
        }
        
        
    }
    //If fireballs hits platform it should delete
    if ((firstBody.categoryBitMask == bossFireCategory)&&
        (secondBody.categoryBitMask == platformCategory))
    {
        //printf("%@ %@ \n", firstBody, secondBody);

        if([firstBody.node.name  isEqualToString: @"bossFire0"]){
            [bossFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire1"]){
            [bossFireBall[1] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire2"]){
            [bossFireBall[2] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire3"]){
            [bossFireBall[3] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire4"]){
            [bossFireBall[4] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"bossFire5"]){
            [bossFireBall[5] removeFromParent];
        }
        
        
    }
    
    
    //Player gets hit by phyiscal boss
    
    if ((firstBody.categoryBitMask == bossCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        //WHY IS THIS BREAKING NOT SURE
        //printf("%@ %@ \n", firstBody, secondBody);
        //NSLog(@" %@ %@ \n", firstBody, secondBody);
        printf("Collison");
        playerHealth --;
        if(playerHealth == 0){
            //gameOver
            NSLog(@"Player died");
            [player removeAllActions];
            [player runAction:death];

            //[player removeFromParent];
        }
        else if(playerHealth > 0){
            [player removeAllActions];
            [player runAction:hit];


            //[player setTexture:celesHit];

            [player.physicsBody applyImpulse:CGVectorMake(-20, 0)];
            
            /*
            if(playerLeft == true){
                [player setTexture: celesLeft];
            }
            else{
                [player setTexture: celesRight];
            }
            */
            
        }
        
    }
    
}

-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    
    cam.position = CGPointMake(player.position.x, CGRectGetMidY(self.frame));
    
    //Keep button on player
    fireButton.position = CGPointMake(player.position.x, -620);
    
    resetButton.position = CGPointMake(player.position.x + 270, -620);
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    
    _lastUpdateTime = currentTime;
    
    if (frameCounter == 30*4 && bossHealth != 0) {
        int patternNum = arc4random_uniform(3);
        printf(" Pattern %d", patternNum);
        if(patternNum == 0){
            [self pattern1];
        }
        if(patternNum == 1){
            [self pattern2];
        }
        if(patternNum == 2){
            [self pattern3];
        }
        
        frameCounter = 0;
    }
    if(dragonBoss.position.x < 200){
        dragonDirection = 1;
    }
    if(dragonBoss.position.x > 400){
        dragonDirection = -1;
    }
    if(frameCounter % 5 == 0){
        [dragonBoss.physicsBody applyImpulse:CGVectorMake(20 * dragonDirection, 0)];
    }
    
    if(bossHealth < 10 && bossPhase == 0){
        [self makePlatform];
        [dragonBoss.physicsBody applyImpulse:CGVectorMake(0, 150)];
        bossPhase = 1;
    }
    
    
    ++frameCounter;
}

@end
