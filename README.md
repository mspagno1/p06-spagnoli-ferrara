You awoke the boss from killing all his minions! Dodge the dragon's fireballs and shoot your own to take down the mighty beast.

Controls: Click to the left or right of the character on the same y level or lower to move left or right. Click to the left or right above the characters current y level to jump in that direction. Can only jump once and it resets once you touched a structure or the ground. Press fire to shoot a fireball in the direction you are facing to kill an imp. Fireballs stop once they hit the dragon. Keep shooting the dragon until he dies or you die. 

Player Health is 5.
Dragon Health is 40.