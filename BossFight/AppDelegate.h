//
//  AppDelegate.h
//  BossFight
//
//  Created by Vaster on 4/6/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

